//
//  main.m
//  RuntimeForIosBasemapScale
//
//  Created by Esri Australia on 22/10/2014.
//  Copyright (c) 2014 Esri Australia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ExampleBaseMapScaleAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ExampleBaseMapScaleAppDelegate class]));
    }
}
