//
//  ExampleBaseMapScaleViewController.m
//  RuntimeForIosBasemapScale
//
//  Created by Esri Australia on 22/10/2014.
//  Copyright (c) 2014 Esri Australia. All rights reserved.
//

#import "ExampleBaseMapScaleViewController.h"


@interface ExampleBaseMapScaleViewController ()



@end

@implementation ExampleBaseMapScaleViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURL* url = [NSURL URLWithString:@"http://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer"];
    AGSTiledMapServiceLayer *basemap = [AGSTiledMapServiceLayer tiledMapServiceLayerWithURL:url];
    basemap.delegate = self;
    [self.agsMapView addMapLayer:basemap withName:@"Basemap Tiled Layer"];
  
}

- (void)layerDidLoad:(AGSLayer *)layer
{
    layer.maxScale = 70.5;//this value should come from a config param
    self.agsMapView.maxScale = 70.5;//this value should come from a config param
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
