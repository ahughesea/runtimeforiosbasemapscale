//
//  ExampleBaseMapScaleAppDelegate.h
//  RuntimeForIosBasemapScale
//
//  Created by Esri Australia on 22/10/2014.
//  Copyright (c) 2014 Esri Australia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExampleBaseMapScaleAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
