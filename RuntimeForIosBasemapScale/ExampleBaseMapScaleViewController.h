//
//  ExampleBaseMapScaleViewController.h
//  RuntimeForIosBasemapScale
//
//  Created by Esri Australia on 22/10/2014.
//  Copyright (c) 2014 Esri Australia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ArcGIS/ArcGIS.h>

@interface ExampleBaseMapScaleViewController : UIViewController<AGSLayerDelegate>
@property (strong, nonatomic) IBOutlet AGSMapView *agsMapView;
@end
